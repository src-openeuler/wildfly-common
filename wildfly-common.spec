Name:             wildfly-common
Version:          1.5.0
Release:          2
Summary:          WildFly common utilities
License:          ASL 2.0
URL:              http://wildfly.org/
Source0:          https://github.com/wildfly/%{name}/archive/%{version}/%{name}-1.5.0.Final.tar.gz
Source1:          https://repo.maven.apache.org/maven2/com/oracle/substratevm/svm/1.0.0-rc9/svm-1.0.0-rc9.pom
BuildRequires:    graphviz maven-local mvn(com.intellij:annotations) mvn(jdepend:jdepend)
BuildRequires:    mvn(org.jboss:jboss-parent:pom:) mvn(org.jboss.apiviz:apiviz) mvn(org.jboss.logging:jboss-logging)
BuildRequires:    mvn(org.jboss.logging:jboss-logging-annotations) mvn(org.jboss.logging:jboss-logging-processor)
BuildRequires:    mvn(org.apache.felix:maven-bundle-plugin) mvn(javax.annotation:javax.annotation-api)
BuildRequires:    mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:    mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:    mvn(org.eclipse.aether:aether-connector-basic)
BuildRequires:    mvn(org.eclipse.aether:aether-transport-wagon)
BuildRequires:    mvn(org.apache.maven.wagon:wagon-http)
BuildRequires:    mvn(org.apache.maven.wagon:wagon-provider-api)
BuildRequires:    java-11-openjdk-devel
Requires:         java-11-openjdk javapackages-tools
BuildArch:        noarch
%description
WildFly is a powerful, modular, & lightweight application server that helps you build amazing applications.


%package    help
Summary:          Documents for %{name}
Buildarch:        noarch
Requires:         man info
Provides:         %{name}-javadoc = %{version}-%{release}
Obsoletes:        %{name}-javadoc < %{version}-%{release}
%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -p1 -n %{name}-%{version}.Final
%pom_remove_parent
%pom_remove_plugin :maven-javadoc-plugin
MVN_JAVADOC_PLUGIN_EXTRA_XML="<configuration>
        <sourceFileExcludes>
          <exclude>**/CommonMessages_*.java</exclude>
          <exclude>**/Log_*.java</exclude>
        </sourceFileExcludes>
      </configuration>"
%pom_add_plugin org.apache.maven.plugins:maven-javadoc-plugin . "$MVN_JAVADOC_PLUGIN_EXTRA_XML"
MVN_DEPENDENCY_PLUGIN_EXTRA_XML="<executions>
          <execution>
            <phase>prepare-package</phase>
            <goals>
              <goal>copy</goal>
            </goals>
            <configuration>
              <artifactItems>
                <artifactItem>
                  <groupId>com.oracle.substratevm</groupId>
                  <artifactId>svm</artifactId>
                  <version>1.0.0-rc9</version>
                  <overWrite>false</overWrite>
                  <outputDirectory>./target</outputDirectory>
                  <destFileName></destFileName>
                </artifactItem>
              </artifactItems>    
            </configuration>
          </execution>
</executions>"
%pom_add_plugin org.apache.maven.plugins:maven-dependency-plugin . "$MVN_DEPENDENCY_PLUGIN_EXTRA_XML"
MVN_COMPILER_PLUGIN_EXTRA_XML="<configuration>
        <source>1.8</source>
        <target>1.8</target>
        <encoding>UTF-8</encoding>
</configuration>"
%pom_add_plugin org.apache.maven.plugins:maven-compiler-plugin . "$MVN_COMPILER_PLUGIN_EXTRA_XML"
%pom_remove_dep org.graalvm.sdk:graal-sdk
%pom_xpath_remove pom:project/pom:dependencies %{SOURCE1}
%mvn_file org.wildfly.common:%{name} %{name}


%build
#set openjdk11 for build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
%mvn_build -b


%install
%mvn_install
install -d -m 0755 %{buildroot}%{_javadir}/svm/
install -pm 0644 target/svm-1.0.0-rc9.jar %{buildroot}%{_javadir}/svm/svm.jar
cp %{SOURCE1} %{buildroot}%{_mavenpomdir}/smv.pom
%add_maven_depmap smv.pom svm/svm.jar  

%files -f .mfiles
%license LICENSE

%files help -f .mfiles-javadoc


%changelog
* Tue Jun 25 2024 Ge Wang <wang__ge@126.com> - 1.5.0-2
- Remove unsupported dependency

* Mon Jun 24 2024 Ge Wang <wang__ge@126.com> - 1.5.0-1
- Update to 1.5.0

* Thu Jun 02 2022 wangkai <wangkai385@h-partners.com> - 1.4.0-1
- Update to 1.4.0

* Fri Dec 20 2019 Shuaishuai Song <songshuaishuai2@huawei.com> - 1.1.0-7
- package init
